<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
	'tt_content',
	'CType',
	[
		'Indexedsearch Helper by Cogtail',
		'c_indexed_search',
		'EXT:c_indexed_search/Resources/Public/Images/Backend/ContentElements/t3foundation_icon.svg'
	]

);
$tca = [
	'types' => [
		'c_indexed_search' => [
			'columnsOverrides' => [
								'bodytext' => [
										'config' => [
											'enableRichtext' => true,
																										// 'richtextConfiguration' => default,
																],
															],
														],
      'showitem' => '
			    --palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
			    header;LLL:EXT:c_indexed_search/Resources/Private/Language/locallang_tca.xlf:tx_Indexedsearch.view,
			    bodytext;LLL:EXT:c_indexed_search/Resources/Private/Language/locallang_tca.xlf:tx_Indexedsearch.hidden,
			    --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
		    	--palette--;;hidden,
			    --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access
                    '
      ],
	  ],
		'columns' => [
			'bodytext' => [
	      'config' => [
             'type' => 'text',
		  ],
	  ],
  ],
];
$GLOBALS['TCA']['tt_content'] = array_replace_recursive($GLOBALS['TCA']['tt_content'], $tca);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'c_indexed_search',
    'Configuration/TypoScript',
    'Cogtail Searchbox Extension'
);
?>
