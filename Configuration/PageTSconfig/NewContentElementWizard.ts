mod.wizards.newContentElement.wizardItems.extra {
        header = ContentElements
        elements {
            c_searchbox {
                iconIdentifier = tx_c_searchbox_icon
                title = Searchbox by Cogtail
                description = Simple Searchbox
                tt_content_defValues {
                    CType = c_searchbox
                }
            }
        }
        show = *
    }
