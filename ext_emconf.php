<?php

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Indexed Search Helper',
    'description' => 'Indexed_search results without wrong html encoding',
    'category' => 'misc',
    'version' => '11.1.0',
    'constraints' => array(
  		'depends' => array(
      'typo3' => '10.4.13-11.9.99',
  		),
  		'conflicts' => array(
  		),
  		'suggests' => array(
  		),
  	),
    'dependencies' => 'cms,extbase,fluid',
	  'state' => 'stable',
    'author' => 'Rene Gast',
);
